package de.szut.store.service;

import de.szut.store.dto.GetArticleInSpecificCurrencyDto;
import de.szut.store.dto.RateDto;
import de.szut.store.exceptionhandling.CurrencycodeNotFoundException;
import de.szut.store.exceptionhandling.ResourceNotFoundException;
import de.szut.store.model.Article;
import de.szut.store.model.Supplier;
import de.szut.store.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ArticleService {
    private final ArticleRepository repository;
    private final SupplierService supplierService;

    public ArticleService(ArticleRepository repository, SupplierService supplierService) {
        this.repository = repository;
        this.supplierService = supplierService;
    }

    public List<Article> readAll(){
        return this.repository.findAll();
    }

    public Article readById(long id){
        Article article = this.repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Article not found on : "+ id));
        return article;
    }

    public Article readByDesignation(String designation){
        Article article = this.repository.findByDesignation(designation);
        if(article == null){
            new ResourceNotFoundException("Article not found on : "+ designation);
        }
        return article;
    }

    public Article createBySupplierId(long supplierId, Article article) {
        Supplier supplier = this.supplierService.readById(supplierId);
        article.setSupplier(supplier);
        supplier.addArticle(article);
        return this.repository.save(article);
    }
}
