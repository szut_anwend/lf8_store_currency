package de.szut.store.dto;

import lombok.Data;

import java.util.Set;

@Data
public class GetAllArticlesBySupplierIdDto {
    private long supplierId;
    private String name;
    private Set<GetArticleDto> allArticles;
}
